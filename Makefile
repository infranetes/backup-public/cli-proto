.PHONY: vendor proto
vendor:
	go mod vendor && go mod tidy

proto:
	@buf generate
	@go generate ./...
	@echo "Generated successfully"
