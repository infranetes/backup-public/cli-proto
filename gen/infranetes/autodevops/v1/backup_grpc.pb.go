// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package v1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// BackupServiceClient is the client API for BackupService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type BackupServiceClient interface {
	FetchBackupToProcess(ctx context.Context, in *FetchBackupToProcessRequest, opts ...grpc.CallOption) (*FetchBackupToProcessResponse, error)
	UpdateBackup(ctx context.Context, in *UpdateBackupRequest, opts ...grpc.CallOption) (*UpdateBackupResponse, error)
}

type backupServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewBackupServiceClient(cc grpc.ClientConnInterface) BackupServiceClient {
	return &backupServiceClient{cc}
}

func (c *backupServiceClient) FetchBackupToProcess(ctx context.Context, in *FetchBackupToProcessRequest, opts ...grpc.CallOption) (*FetchBackupToProcessResponse, error) {
	out := new(FetchBackupToProcessResponse)
	err := c.cc.Invoke(ctx, "/api.infranetes.autodevops.v1.BackupService/FetchBackupToProcess", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *backupServiceClient) UpdateBackup(ctx context.Context, in *UpdateBackupRequest, opts ...grpc.CallOption) (*UpdateBackupResponse, error) {
	out := new(UpdateBackupResponse)
	err := c.cc.Invoke(ctx, "/api.infranetes.autodevops.v1.BackupService/UpdateBackup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// BackupServiceServer is the server API for BackupService service.
// All implementations should embed UnimplementedBackupServiceServer
// for forward compatibility
type BackupServiceServer interface {
	FetchBackupToProcess(context.Context, *FetchBackupToProcessRequest) (*FetchBackupToProcessResponse, error)
	UpdateBackup(context.Context, *UpdateBackupRequest) (*UpdateBackupResponse, error)
}

// UnimplementedBackupServiceServer should be embedded to have forward compatible implementations.
type UnimplementedBackupServiceServer struct {
}

func (UnimplementedBackupServiceServer) FetchBackupToProcess(context.Context, *FetchBackupToProcessRequest) (*FetchBackupToProcessResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FetchBackupToProcess not implemented")
}
func (UnimplementedBackupServiceServer) UpdateBackup(context.Context, *UpdateBackupRequest) (*UpdateBackupResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateBackup not implemented")
}

// UnsafeBackupServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to BackupServiceServer will
// result in compilation errors.
type UnsafeBackupServiceServer interface {
	mustEmbedUnimplementedBackupServiceServer()
}

func RegisterBackupServiceServer(s grpc.ServiceRegistrar, srv BackupServiceServer) {
	s.RegisterService(&BackupService_ServiceDesc, srv)
}

func _BackupService_FetchBackupToProcess_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FetchBackupToProcessRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BackupServiceServer).FetchBackupToProcess(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.infranetes.autodevops.v1.BackupService/FetchBackupToProcess",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BackupServiceServer).FetchBackupToProcess(ctx, req.(*FetchBackupToProcessRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _BackupService_UpdateBackup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateBackupRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BackupServiceServer).UpdateBackup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.infranetes.autodevops.v1.BackupService/UpdateBackup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BackupServiceServer).UpdateBackup(ctx, req.(*UpdateBackupRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// BackupService_ServiceDesc is the grpc.ServiceDesc for BackupService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var BackupService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "api.infranetes.autodevops.v1.BackupService",
	HandlerType: (*BackupServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "FetchBackupToProcess",
			Handler:    _BackupService_FetchBackupToProcess_Handler,
		},
		{
			MethodName: "UpdateBackup",
			Handler:    _BackupService_UpdateBackup_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "infranetes/autodevops/v1/backup.proto",
}
