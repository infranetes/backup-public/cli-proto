// Code generated by MockGen. DO NOT EDIT.
// Source: vault_grpc.pb.go

// Package mocks is a generated GoMock package.
package mocks

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	v1 "gitlab.com/infranetes/backup/proto/gen/infranetes/autodevops/v1"
	grpc "google.golang.org/grpc"
)

// MockVaultClient is a mock of VaultClient interface.
type MockVaultClient struct {
	ctrl     *gomock.Controller
	recorder *MockVaultClientMockRecorder
}

// MockVaultClientMockRecorder is the mock recorder for MockVaultClient.
type MockVaultClientMockRecorder struct {
	mock *MockVaultClient
}

// NewMockVaultClient creates a new mock instance.
func NewMockVaultClient(ctrl *gomock.Controller) *MockVaultClient {
	mock := &MockVaultClient{ctrl: ctrl}
	mock.recorder = &MockVaultClientMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockVaultClient) EXPECT() *MockVaultClientMockRecorder {
	return m.recorder
}

// FetchVaultExport mocks base method.
func (m *MockVaultClient) FetchVaultExport(ctx context.Context, in *v1.FetchVaultExportRequest, opts ...grpc.CallOption) (*v1.FetchVaultExportResponse, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{ctx, in}
	for _, a := range opts {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "FetchVaultExport", varargs...)
	ret0, _ := ret[0].(*v1.FetchVaultExportResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FetchVaultExport indicates an expected call of FetchVaultExport.
func (mr *MockVaultClientMockRecorder) FetchVaultExport(ctx, in interface{}, opts ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{ctx, in}, opts...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FetchVaultExport", reflect.TypeOf((*MockVaultClient)(nil).FetchVaultExport), varargs...)
}

// FetchVaultRestore mocks base method.
func (m *MockVaultClient) FetchVaultRestore(ctx context.Context, in *v1.FetchVaultRestoreRequest, opts ...grpc.CallOption) (*v1.FetchVaultRestoreResponse, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{ctx, in}
	for _, a := range opts {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "FetchVaultRestore", varargs...)
	ret0, _ := ret[0].(*v1.FetchVaultRestoreResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FetchVaultRestore indicates an expected call of FetchVaultRestore.
func (mr *MockVaultClientMockRecorder) FetchVaultRestore(ctx, in interface{}, opts ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{ctx, in}, opts...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FetchVaultRestore", reflect.TypeOf((*MockVaultClient)(nil).FetchVaultRestore), varargs...)
}

// UpdateVaultExport mocks base method.
func (m *MockVaultClient) UpdateVaultExport(ctx context.Context, in *v1.UpdateVaultExportRequest, opts ...grpc.CallOption) (*v1.UpdateVaultExportResponse, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{ctx, in}
	for _, a := range opts {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "UpdateVaultExport", varargs...)
	ret0, _ := ret[0].(*v1.UpdateVaultExportResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// UpdateVaultExport indicates an expected call of UpdateVaultExport.
func (mr *MockVaultClientMockRecorder) UpdateVaultExport(ctx, in interface{}, opts ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{ctx, in}, opts...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateVaultExport", reflect.TypeOf((*MockVaultClient)(nil).UpdateVaultExport), varargs...)
}

// UpdateVaultRestore mocks base method.
func (m *MockVaultClient) UpdateVaultRestore(ctx context.Context, in *v1.UpdateVaultRestoreRequest, opts ...grpc.CallOption) (*v1.UpdateVaultRestoreResponse, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{ctx, in}
	for _, a := range opts {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "UpdateVaultRestore", varargs...)
	ret0, _ := ret[0].(*v1.UpdateVaultRestoreResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// UpdateVaultRestore indicates an expected call of UpdateVaultRestore.
func (mr *MockVaultClientMockRecorder) UpdateVaultRestore(ctx, in interface{}, opts ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{ctx, in}, opts...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateVaultRestore", reflect.TypeOf((*MockVaultClient)(nil).UpdateVaultRestore), varargs...)
}

// MockVaultServer is a mock of VaultServer interface.
type MockVaultServer struct {
	ctrl     *gomock.Controller
	recorder *MockVaultServerMockRecorder
}

// MockVaultServerMockRecorder is the mock recorder for MockVaultServer.
type MockVaultServerMockRecorder struct {
	mock *MockVaultServer
}

// NewMockVaultServer creates a new mock instance.
func NewMockVaultServer(ctrl *gomock.Controller) *MockVaultServer {
	mock := &MockVaultServer{ctrl: ctrl}
	mock.recorder = &MockVaultServerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockVaultServer) EXPECT() *MockVaultServerMockRecorder {
	return m.recorder
}

// FetchVaultExport mocks base method.
func (m *MockVaultServer) FetchVaultExport(arg0 context.Context, arg1 *v1.FetchVaultExportRequest) (*v1.FetchVaultExportResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FetchVaultExport", arg0, arg1)
	ret0, _ := ret[0].(*v1.FetchVaultExportResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FetchVaultExport indicates an expected call of FetchVaultExport.
func (mr *MockVaultServerMockRecorder) FetchVaultExport(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FetchVaultExport", reflect.TypeOf((*MockVaultServer)(nil).FetchVaultExport), arg0, arg1)
}

// FetchVaultRestore mocks base method.
func (m *MockVaultServer) FetchVaultRestore(arg0 context.Context, arg1 *v1.FetchVaultRestoreRequest) (*v1.FetchVaultRestoreResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FetchVaultRestore", arg0, arg1)
	ret0, _ := ret[0].(*v1.FetchVaultRestoreResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FetchVaultRestore indicates an expected call of FetchVaultRestore.
func (mr *MockVaultServerMockRecorder) FetchVaultRestore(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FetchVaultRestore", reflect.TypeOf((*MockVaultServer)(nil).FetchVaultRestore), arg0, arg1)
}

// UpdateVaultExport mocks base method.
func (m *MockVaultServer) UpdateVaultExport(arg0 context.Context, arg1 *v1.UpdateVaultExportRequest) (*v1.UpdateVaultExportResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateVaultExport", arg0, arg1)
	ret0, _ := ret[0].(*v1.UpdateVaultExportResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// UpdateVaultExport indicates an expected call of UpdateVaultExport.
func (mr *MockVaultServerMockRecorder) UpdateVaultExport(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateVaultExport", reflect.TypeOf((*MockVaultServer)(nil).UpdateVaultExport), arg0, arg1)
}

// UpdateVaultRestore mocks base method.
func (m *MockVaultServer) UpdateVaultRestore(arg0 context.Context, arg1 *v1.UpdateVaultRestoreRequest) (*v1.UpdateVaultRestoreResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateVaultRestore", arg0, arg1)
	ret0, _ := ret[0].(*v1.UpdateVaultRestoreResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// UpdateVaultRestore indicates an expected call of UpdateVaultRestore.
func (mr *MockVaultServerMockRecorder) UpdateVaultRestore(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateVaultRestore", reflect.TypeOf((*MockVaultServer)(nil).UpdateVaultRestore), arg0, arg1)
}

// MockUnsafeVaultServer is a mock of UnsafeVaultServer interface.
type MockUnsafeVaultServer struct {
	ctrl     *gomock.Controller
	recorder *MockUnsafeVaultServerMockRecorder
}

// MockUnsafeVaultServerMockRecorder is the mock recorder for MockUnsafeVaultServer.
type MockUnsafeVaultServerMockRecorder struct {
	mock *MockUnsafeVaultServer
}

// NewMockUnsafeVaultServer creates a new mock instance.
func NewMockUnsafeVaultServer(ctrl *gomock.Controller) *MockUnsafeVaultServer {
	mock := &MockUnsafeVaultServer{ctrl: ctrl}
	mock.recorder = &MockUnsafeVaultServerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockUnsafeVaultServer) EXPECT() *MockUnsafeVaultServerMockRecorder {
	return m.recorder
}

// mustEmbedUnimplementedVaultServer mocks base method.
func (m *MockUnsafeVaultServer) mustEmbedUnimplementedVaultServer() {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "mustEmbedUnimplementedVaultServer")
}

// mustEmbedUnimplementedVaultServer indicates an expected call of mustEmbedUnimplementedVaultServer.
func (mr *MockUnsafeVaultServerMockRecorder) mustEmbedUnimplementedVaultServer() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "mustEmbedUnimplementedVaultServer", reflect.TypeOf((*MockUnsafeVaultServer)(nil).mustEmbedUnimplementedVaultServer))
}
