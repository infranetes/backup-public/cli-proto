//go:generate mockgen -package mocks -source backup_grpc.pb.go -destination mocks/backup.go

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.19.1
// source: infranetes/autodevops/v1/backup.proto

package v1

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type FetchBackupToProcessRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Cluster string `protobuf:"bytes,1,opt,name=cluster,proto3" json:"cluster,omitempty"`
}

func (x *FetchBackupToProcessRequest) Reset() {
	*x = FetchBackupToProcessRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FetchBackupToProcessRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FetchBackupToProcessRequest) ProtoMessage() {}

func (x *FetchBackupToProcessRequest) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FetchBackupToProcessRequest.ProtoReflect.Descriptor instead.
func (*FetchBackupToProcessRequest) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{0}
}

func (x *FetchBackupToProcessRequest) GetCluster() string {
	if x != nil {
		return x.Cluster
	}
	return ""
}

type FetchBackup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Namespace     string `protobuf:"bytes,2,opt,name=namespace,proto3" json:"namespace,omitempty"`
	SnapshotClass string `protobuf:"bytes,3,opt,name=snapshot_class,json=snapshotClass,proto3" json:"snapshot_class,omitempty"`
}

func (x *FetchBackup) Reset() {
	*x = FetchBackup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FetchBackup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FetchBackup) ProtoMessage() {}

func (x *FetchBackup) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FetchBackup.ProtoReflect.Descriptor instead.
func (*FetchBackup) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{1}
}

func (x *FetchBackup) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *FetchBackup) GetNamespace() string {
	if x != nil {
		return x.Namespace
	}
	return ""
}

func (x *FetchBackup) GetSnapshotClass() string {
	if x != nil {
		return x.SnapshotClass
	}
	return ""
}

type FetchBackupToProcessResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Backup *FetchBackup `protobuf:"bytes,1,opt,name=backup,proto3" json:"backup,omitempty"`
}

func (x *FetchBackupToProcessResponse) Reset() {
	*x = FetchBackupToProcessResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FetchBackupToProcessResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FetchBackupToProcessResponse) ProtoMessage() {}

func (x *FetchBackupToProcessResponse) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FetchBackupToProcessResponse.ProtoReflect.Descriptor instead.
func (*FetchBackupToProcessResponse) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{2}
}

func (x *FetchBackupToProcessResponse) GetBackup() *FetchBackup {
	if x != nil {
		return x.Backup
	}
	return nil
}

type UpdateBackupPvc struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name         string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Phase        string `protobuf:"bytes,2,opt,name=phase,proto3" json:"phase,omitempty"`
	Description  string `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	SnapshotName string `protobuf:"bytes,4,opt,name=snapshot_name,json=snapshotName,proto3" json:"snapshot_name,omitempty"`
	ContentSpec  string `protobuf:"bytes,5,opt,name=content_spec,json=contentSpec,proto3" json:"content_spec,omitempty"`
	Spec         string `protobuf:"bytes,6,opt,name=spec,proto3" json:"spec,omitempty"`
}

func (x *UpdateBackupPvc) Reset() {
	*x = UpdateBackupPvc{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateBackupPvc) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateBackupPvc) ProtoMessage() {}

func (x *UpdateBackupPvc) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateBackupPvc.ProtoReflect.Descriptor instead.
func (*UpdateBackupPvc) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateBackupPvc) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *UpdateBackupPvc) GetPhase() string {
	if x != nil {
		return x.Phase
	}
	return ""
}

func (x *UpdateBackupPvc) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *UpdateBackupPvc) GetSnapshotName() string {
	if x != nil {
		return x.SnapshotName
	}
	return ""
}

func (x *UpdateBackupPvc) GetContentSpec() string {
	if x != nil {
		return x.ContentSpec
	}
	return ""
}

func (x *UpdateBackupPvc) GetSpec() string {
	if x != nil {
		return x.Spec
	}
	return ""
}

type UpdateBackupRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string                 `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Phase       string                 `protobuf:"bytes,2,opt,name=phase,proto3" json:"phase,omitempty"`
	Description string                 `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	Pvcs        []*UpdateBackupPvc     `protobuf:"bytes,4,rep,name=pvcs,proto3" json:"pvcs,omitempty"`
	StartedAt   *timestamppb.Timestamp `protobuf:"bytes,5,opt,name=started_at,json=startedAt,proto3" json:"started_at,omitempty"`
	FinishedAt  *timestamppb.Timestamp `protobuf:"bytes,6,opt,name=finished_at,json=finishedAt,proto3" json:"finished_at,omitempty"`
}

func (x *UpdateBackupRequest) Reset() {
	*x = UpdateBackupRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateBackupRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateBackupRequest) ProtoMessage() {}

func (x *UpdateBackupRequest) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateBackupRequest.ProtoReflect.Descriptor instead.
func (*UpdateBackupRequest) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{4}
}

func (x *UpdateBackupRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateBackupRequest) GetPhase() string {
	if x != nil {
		return x.Phase
	}
	return ""
}

func (x *UpdateBackupRequest) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *UpdateBackupRequest) GetPvcs() []*UpdateBackupPvc {
	if x != nil {
		return x.Pvcs
	}
	return nil
}

func (x *UpdateBackupRequest) GetStartedAt() *timestamppb.Timestamp {
	if x != nil {
		return x.StartedAt
	}
	return nil
}

func (x *UpdateBackupRequest) GetFinishedAt() *timestamppb.Timestamp {
	if x != nil {
		return x.FinishedAt
	}
	return nil
}

type UpdateBackupResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *UpdateBackupResponse) Reset() {
	*x = UpdateBackupResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateBackupResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateBackupResponse) ProtoMessage() {}

func (x *UpdateBackupResponse) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateBackupResponse.ProtoReflect.Descriptor instead.
func (*UpdateBackupResponse) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{5}
}

type BackupPvc struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name         string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Phase        string `protobuf:"bytes,2,opt,name=phase,proto3" json:"phase,omitempty"`
	Description  string `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	SnapshotName string `protobuf:"bytes,4,opt,name=snapshot_name,json=snapshotName,proto3" json:"snapshot_name,omitempty"`
	ContentSpec  string `protobuf:"bytes,5,opt,name=content_spec,json=contentSpec,proto3" json:"content_spec,omitempty"`
	Spec         string `protobuf:"bytes,6,opt,name=spec,proto3" json:"spec,omitempty"`
}

func (x *BackupPvc) Reset() {
	*x = BackupPvc{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BackupPvc) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BackupPvc) ProtoMessage() {}

func (x *BackupPvc) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BackupPvc.ProtoReflect.Descriptor instead.
func (*BackupPvc) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{6}
}

func (x *BackupPvc) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *BackupPvc) GetPhase() string {
	if x != nil {
		return x.Phase
	}
	return ""
}

func (x *BackupPvc) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *BackupPvc) GetSnapshotName() string {
	if x != nil {
		return x.SnapshotName
	}
	return ""
}

func (x *BackupPvc) GetContentSpec() string {
	if x != nil {
		return x.ContentSpec
	}
	return ""
}

func (x *BackupPvc) GetSpec() string {
	if x != nil {
		return x.Spec
	}
	return ""
}

type Backup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string                 `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Phase        string                 `protobuf:"bytes,2,opt,name=phase,proto3" json:"phase,omitempty"`
	Description  string                 `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	Cluster      string                 `protobuf:"bytes,4,opt,name=cluster,proto3" json:"cluster,omitempty"`
	Env          string                 `protobuf:"bytes,5,opt,name=env,proto3" json:"env,omitempty"`
	BackupPlanId string                 `protobuf:"bytes,6,opt,name=backupPlanId,proto3" json:"backupPlanId,omitempty"`
	Pvcs         []*BackupPvc           `protobuf:"bytes,7,rep,name=pvcs,proto3" json:"pvcs,omitempty"`
	CreatedAt    *timestamppb.Timestamp `protobuf:"bytes,8,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	StartedAt    *timestamppb.Timestamp `protobuf:"bytes,9,opt,name=started_at,json=startedAt,proto3" json:"started_at,omitempty"`
	FinishedAt   *timestamppb.Timestamp `protobuf:"bytes,10,opt,name=finished_at,json=finishedAt,proto3" json:"finished_at,omitempty"`
}

func (x *Backup) Reset() {
	*x = Backup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Backup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Backup) ProtoMessage() {}

func (x *Backup) ProtoReflect() protoreflect.Message {
	mi := &file_infranetes_autodevops_v1_backup_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Backup.ProtoReflect.Descriptor instead.
func (*Backup) Descriptor() ([]byte, []int) {
	return file_infranetes_autodevops_v1_backup_proto_rawDescGZIP(), []int{7}
}

func (x *Backup) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Backup) GetPhase() string {
	if x != nil {
		return x.Phase
	}
	return ""
}

func (x *Backup) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *Backup) GetCluster() string {
	if x != nil {
		return x.Cluster
	}
	return ""
}

func (x *Backup) GetEnv() string {
	if x != nil {
		return x.Env
	}
	return ""
}

func (x *Backup) GetBackupPlanId() string {
	if x != nil {
		return x.BackupPlanId
	}
	return ""
}

func (x *Backup) GetPvcs() []*BackupPvc {
	if x != nil {
		return x.Pvcs
	}
	return nil
}

func (x *Backup) GetCreatedAt() *timestamppb.Timestamp {
	if x != nil {
		return x.CreatedAt
	}
	return nil
}

func (x *Backup) GetStartedAt() *timestamppb.Timestamp {
	if x != nil {
		return x.StartedAt
	}
	return nil
}

func (x *Backup) GetFinishedAt() *timestamppb.Timestamp {
	if x != nil {
		return x.FinishedAt
	}
	return nil
}

var File_infranetes_autodevops_v1_backup_proto protoreflect.FileDescriptor

var file_infranetes_autodevops_v1_backup_proto_rawDesc = []byte{
	0x0a, 0x25, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x6e, 0x65, 0x74, 0x65, 0x73, 0x2f, 0x61, 0x75, 0x74,
	0x6f, 0x64, 0x65, 0x76, 0x6f, 0x70, 0x73, 0x2f, 0x76, 0x31, 0x2f, 0x62, 0x61, 0x63, 0x6b, 0x75,
	0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1c, 0x61, 0x70, 0x69, 0x2e, 0x69, 0x6e, 0x66,
	0x72, 0x61, 0x6e, 0x65, 0x74, 0x65, 0x73, 0x2e, 0x61, 0x75, 0x74, 0x6f, 0x64, 0x65, 0x76, 0x6f,
	0x70, 0x73, 0x2e, 0x76, 0x31, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x37, 0x0a, 0x1b, 0x46, 0x65, 0x74, 0x63, 0x68, 0x42,
	0x61, 0x63, 0x6b, 0x75, 0x70, 0x54, 0x6f, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x63, 0x6c, 0x75, 0x73, 0x74, 0x65, 0x72,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x63, 0x6c, 0x75, 0x73, 0x74, 0x65, 0x72, 0x22,
	0x62, 0x0a, 0x0b, 0x46, 0x65, 0x74, 0x63, 0x68, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1c,
	0x0a, 0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x12, 0x25, 0x0a, 0x0e,
	0x73, 0x6e, 0x61, 0x70, 0x73, 0x68, 0x6f, 0x74, 0x5f, 0x63, 0x6c, 0x61, 0x73, 0x73, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x73, 0x6e, 0x61, 0x70, 0x73, 0x68, 0x6f, 0x74, 0x43, 0x6c,
	0x61, 0x73, 0x73, 0x22, 0x61, 0x0a, 0x1c, 0x46, 0x65, 0x74, 0x63, 0x68, 0x42, 0x61, 0x63, 0x6b,
	0x75, 0x70, 0x54, 0x6f, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x41, 0x0a, 0x06, 0x62, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x29, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x6e,
	0x65, 0x74, 0x65, 0x73, 0x2e, 0x61, 0x75, 0x74, 0x6f, 0x64, 0x65, 0x76, 0x6f, 0x70, 0x73, 0x2e,
	0x76, 0x31, 0x2e, 0x46, 0x65, 0x74, 0x63, 0x68, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x52, 0x06,
	0x62, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x22, 0xb9, 0x01, 0x0a, 0x0f, 0x55, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x50, 0x76, 0x63, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61,
	0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x14,
	0x0a, 0x05, 0x70, 0x68, 0x61, 0x73, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70,
	0x68, 0x61, 0x73, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74,
	0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72,
	0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x23, 0x0a, 0x0d, 0x73, 0x6e, 0x61, 0x70, 0x73, 0x68,
	0x6f, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x73,
	0x6e, 0x61, 0x70, 0x73, 0x68, 0x6f, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x21, 0x0a, 0x0c, 0x63,
	0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x5f, 0x73, 0x70, 0x65, 0x63, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x53, 0x70, 0x65, 0x63, 0x12, 0x12,
	0x0a, 0x04, 0x73, 0x70, 0x65, 0x63, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x73, 0x70,
	0x65, 0x63, 0x22, 0x98, 0x02, 0x0a, 0x13, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x61, 0x63,
	0x6b, 0x75, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68,
	0x61, 0x73, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x61, 0x73, 0x65,
	0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x12, 0x41, 0x0a, 0x04, 0x70, 0x76, 0x63, 0x73, 0x18, 0x04, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x2d, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x6e, 0x65, 0x74, 0x65,
	0x73, 0x2e, 0x61, 0x75, 0x74, 0x6f, 0x64, 0x65, 0x76, 0x6f, 0x70, 0x73, 0x2e, 0x76, 0x31, 0x2e,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x50, 0x76, 0x63, 0x52,
	0x04, 0x70, 0x76, 0x63, 0x73, 0x12, 0x39, 0x0a, 0x0a, 0x73, 0x74, 0x61, 0x72, 0x74, 0x65, 0x64,
	0x5f, 0x61, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65,
	0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x73, 0x74, 0x61, 0x72, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x12, 0x3b, 0x0a, 0x0b, 0x66, 0x69, 0x6e, 0x69, 0x73, 0x68, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d,
	0x70, 0x52, 0x0a, 0x66, 0x69, 0x6e, 0x69, 0x73, 0x68, 0x65, 0x64, 0x41, 0x74, 0x22, 0x16, 0x0a,
	0x14, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0xb3, 0x01, 0x0a, 0x09, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70,
	0x50, 0x76, 0x63, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x61, 0x73, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x61, 0x73, 0x65, 0x12, 0x20, 0x0a,
	0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x23, 0x0a, 0x0d, 0x73, 0x6e, 0x61, 0x70, 0x73, 0x68, 0x6f, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x73, 0x6e, 0x61, 0x70, 0x73, 0x68, 0x6f, 0x74,
	0x4e, 0x61, 0x6d, 0x65, 0x12, 0x21, 0x0a, 0x0c, 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x5f,
	0x73, 0x70, 0x65, 0x63, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x63, 0x6f, 0x6e, 0x74,
	0x65, 0x6e, 0x74, 0x53, 0x70, 0x65, 0x63, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x70, 0x65, 0x63, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x73, 0x70, 0x65, 0x63, 0x22, 0x90, 0x03, 0x0a, 0x06,
	0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x61, 0x73, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x61, 0x73, 0x65, 0x12, 0x20, 0x0a, 0x0b,
	0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x18,
	0x0a, 0x07, 0x63, 0x6c, 0x75, 0x73, 0x74, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x07, 0x63, 0x6c, 0x75, 0x73, 0x74, 0x65, 0x72, 0x12, 0x10, 0x0a, 0x03, 0x65, 0x6e, 0x76, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x65, 0x6e, 0x76, 0x12, 0x22, 0x0a, 0x0c, 0x62, 0x61,
	0x63, 0x6b, 0x75, 0x70, 0x50, 0x6c, 0x61, 0x6e, 0x49, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0c, 0x62, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x50, 0x6c, 0x61, 0x6e, 0x49, 0x64, 0x12, 0x3b,
	0x0a, 0x04, 0x70, 0x76, 0x63, 0x73, 0x18, 0x07, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x27, 0x2e, 0x61,
	0x70, 0x69, 0x2e, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x6e, 0x65, 0x74, 0x65, 0x73, 0x2e, 0x61, 0x75,
	0x74, 0x6f, 0x64, 0x65, 0x76, 0x6f, 0x70, 0x73, 0x2e, 0x76, 0x31, 0x2e, 0x42, 0x61, 0x63, 0x6b,
	0x75, 0x70, 0x50, 0x76, 0x63, 0x52, 0x04, 0x70, 0x76, 0x63, 0x73, 0x12, 0x39, 0x0a, 0x0a, 0x63,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x63, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x39, 0x0a, 0x0a, 0x73, 0x74, 0x61, 0x72, 0x74, 0x65,
	0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d,
	0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x73, 0x74, 0x61, 0x72, 0x74, 0x65, 0x64, 0x41,
	0x74, 0x12, 0x3b, 0x0a, 0x0b, 0x66, 0x69, 0x6e, 0x69, 0x73, 0x68, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x0a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61,
	0x6d, 0x70, 0x52, 0x0a, 0x66, 0x69, 0x6e, 0x69, 0x73, 0x68, 0x65, 0x64, 0x41, 0x74, 0x32, 0x9a,
	0x02, 0x0a, 0x0d, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x12, 0x8f, 0x01, 0x0a, 0x14, 0x46, 0x65, 0x74, 0x63, 0x68, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70,
	0x54, 0x6f, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x12, 0x39, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x69, 0x6e, 0x66, 0x72, 0x61, 0x6e, 0x65, 0x74, 0x65, 0x73, 0x2e, 0x61, 0x75, 0x74, 0x6f, 0x64,
	0x65, 0x76, 0x6f, 0x70, 0x73, 0x2e, 0x76, 0x31, 0x2e, 0x46, 0x65, 0x74, 0x63, 0x68, 0x42, 0x61,
	0x63, 0x6b, 0x75, 0x70, 0x54, 0x6f, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x3a, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x69, 0x6e, 0x66, 0x72, 0x61,
	0x6e, 0x65, 0x74, 0x65, 0x73, 0x2e, 0x61, 0x75, 0x74, 0x6f, 0x64, 0x65, 0x76, 0x6f, 0x70, 0x73,
	0x2e, 0x76, 0x31, 0x2e, 0x46, 0x65, 0x74, 0x63, 0x68, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x54,
	0x6f, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x12, 0x77, 0x0a, 0x0c, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x61, 0x63, 0x6b,
	0x75, 0x70, 0x12, 0x31, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x6e, 0x65,
	0x74, 0x65, 0x73, 0x2e, 0x61, 0x75, 0x74, 0x6f, 0x64, 0x65, 0x76, 0x6f, 0x70, 0x73, 0x2e, 0x76,
	0x31, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x32, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x69, 0x6e, 0x66, 0x72,
	0x61, 0x6e, 0x65, 0x74, 0x65, 0x73, 0x2e, 0x61, 0x75, 0x74, 0x6f, 0x64, 0x65, 0x76, 0x6f, 0x70,
	0x73, 0x2e, 0x76, 0x31, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x61, 0x63, 0x6b, 0x75,
	0x70, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x27, 0x5a, 0x25, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x6e,
	0x65, 0x74, 0x65, 0x73, 0x2f, 0x62, 0x61, 0x63, 0x6b, 0x75, 0x70, 0x2f, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2f, 0x76, 0x31, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_infranetes_autodevops_v1_backup_proto_rawDescOnce sync.Once
	file_infranetes_autodevops_v1_backup_proto_rawDescData = file_infranetes_autodevops_v1_backup_proto_rawDesc
)

func file_infranetes_autodevops_v1_backup_proto_rawDescGZIP() []byte {
	file_infranetes_autodevops_v1_backup_proto_rawDescOnce.Do(func() {
		file_infranetes_autodevops_v1_backup_proto_rawDescData = protoimpl.X.CompressGZIP(file_infranetes_autodevops_v1_backup_proto_rawDescData)
	})
	return file_infranetes_autodevops_v1_backup_proto_rawDescData
}

var file_infranetes_autodevops_v1_backup_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_infranetes_autodevops_v1_backup_proto_goTypes = []interface{}{
	(*FetchBackupToProcessRequest)(nil),  // 0: api.infranetes.autodevops.v1.FetchBackupToProcessRequest
	(*FetchBackup)(nil),                  // 1: api.infranetes.autodevops.v1.FetchBackup
	(*FetchBackupToProcessResponse)(nil), // 2: api.infranetes.autodevops.v1.FetchBackupToProcessResponse
	(*UpdateBackupPvc)(nil),              // 3: api.infranetes.autodevops.v1.UpdateBackupPvc
	(*UpdateBackupRequest)(nil),          // 4: api.infranetes.autodevops.v1.UpdateBackupRequest
	(*UpdateBackupResponse)(nil),         // 5: api.infranetes.autodevops.v1.UpdateBackupResponse
	(*BackupPvc)(nil),                    // 6: api.infranetes.autodevops.v1.BackupPvc
	(*Backup)(nil),                       // 7: api.infranetes.autodevops.v1.Backup
	(*timestamppb.Timestamp)(nil),        // 8: google.protobuf.Timestamp
}
var file_infranetes_autodevops_v1_backup_proto_depIdxs = []int32{
	1,  // 0: api.infranetes.autodevops.v1.FetchBackupToProcessResponse.backup:type_name -> api.infranetes.autodevops.v1.FetchBackup
	3,  // 1: api.infranetes.autodevops.v1.UpdateBackupRequest.pvcs:type_name -> api.infranetes.autodevops.v1.UpdateBackupPvc
	8,  // 2: api.infranetes.autodevops.v1.UpdateBackupRequest.started_at:type_name -> google.protobuf.Timestamp
	8,  // 3: api.infranetes.autodevops.v1.UpdateBackupRequest.finished_at:type_name -> google.protobuf.Timestamp
	6,  // 4: api.infranetes.autodevops.v1.Backup.pvcs:type_name -> api.infranetes.autodevops.v1.BackupPvc
	8,  // 5: api.infranetes.autodevops.v1.Backup.created_at:type_name -> google.protobuf.Timestamp
	8,  // 6: api.infranetes.autodevops.v1.Backup.started_at:type_name -> google.protobuf.Timestamp
	8,  // 7: api.infranetes.autodevops.v1.Backup.finished_at:type_name -> google.protobuf.Timestamp
	0,  // 8: api.infranetes.autodevops.v1.BackupService.FetchBackupToProcess:input_type -> api.infranetes.autodevops.v1.FetchBackupToProcessRequest
	4,  // 9: api.infranetes.autodevops.v1.BackupService.UpdateBackup:input_type -> api.infranetes.autodevops.v1.UpdateBackupRequest
	2,  // 10: api.infranetes.autodevops.v1.BackupService.FetchBackupToProcess:output_type -> api.infranetes.autodevops.v1.FetchBackupToProcessResponse
	5,  // 11: api.infranetes.autodevops.v1.BackupService.UpdateBackup:output_type -> api.infranetes.autodevops.v1.UpdateBackupResponse
	10, // [10:12] is the sub-list for method output_type
	8,  // [8:10] is the sub-list for method input_type
	8,  // [8:8] is the sub-list for extension type_name
	8,  // [8:8] is the sub-list for extension extendee
	0,  // [0:8] is the sub-list for field type_name
}

func init() { file_infranetes_autodevops_v1_backup_proto_init() }
func file_infranetes_autodevops_v1_backup_proto_init() {
	if File_infranetes_autodevops_v1_backup_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_infranetes_autodevops_v1_backup_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FetchBackupToProcessRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infranetes_autodevops_v1_backup_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FetchBackup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infranetes_autodevops_v1_backup_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FetchBackupToProcessResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infranetes_autodevops_v1_backup_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateBackupPvc); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infranetes_autodevops_v1_backup_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateBackupRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infranetes_autodevops_v1_backup_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateBackupResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infranetes_autodevops_v1_backup_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BackupPvc); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infranetes_autodevops_v1_backup_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Backup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_infranetes_autodevops_v1_backup_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_infranetes_autodevops_v1_backup_proto_goTypes,
		DependencyIndexes: file_infranetes_autodevops_v1_backup_proto_depIdxs,
		MessageInfos:      file_infranetes_autodevops_v1_backup_proto_msgTypes,
	}.Build()
	File_infranetes_autodevops_v1_backup_proto = out.File
	file_infranetes_autodevops_v1_backup_proto_rawDesc = nil
	file_infranetes_autodevops_v1_backup_proto_goTypes = nil
	file_infranetes_autodevops_v1_backup_proto_depIdxs = nil
}
