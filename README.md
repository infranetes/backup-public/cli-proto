## Infranetes API

### Content

1. infranetes/autodevops
   
Contains API contract to communicate operator with api server.

2. infranetes/ui 

Contains API contract to handle requests from frontend.

> 💡 Remember to include go:generate mockgen in your proto files

### Prerequisites

> source https://docs.buf.build/installation/

Install `buf` CLI for Linux/Mac (root privileges are needed)

Install `protoc`: http://google.github.io/proto-lens/installing-protoc.html

Install `mockgen` for Linux/Mac. Version specified in go.mod 

Install `protoc-gen-grpc-web`: https://github.com/grpc/grpc-web#code-generator-plugin. MAC hint: go to Security and Privacy and allow running this plugin

Run: 
```
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.26
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1
```

```
BIN="/usr/local/bin" && \
VERSION="1.0.0-rc1" && \
BINARY_NAME="buf" && \
  curl -sSL \
    "https://github.com/bufbuild/buf/releases/download/v${VERSION}/${BINARY_NAME}-$(uname -s)-$(uname -m)" \
    -o "${BIN}/${BINARY_NAME}" && \
  chmod +x "${BIN}/${BINARY_NAME}"
```

### Generate GO code

```
make proto
```